---
title:  Deploying StreamCardano to K8S
author: Muhammad Hassan
---

# Kubernetes

This directory contain yamls files for all services listed in docker-compose.yml 

## Localhost testing of kubernetes deployment

Following testing has been done on single node kubernetes cluster (Tested on EC2 type t3.2xlarge ubuntu)

Make sure docker and kubernetes install on machine
```bash
sudo apt-get install docker.io
sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni
export KUBECONFIG=/etc/kubernetes/admin.conf
```


### Initialize kubernetes cluster and initilize pod network
```bash
kubeadm init
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```

### Verify master node should be ready
```bash
kubectl get nodes
```

Note: At this point we can add more nodes, depends on need and size of machine/nodes

## Deploy all services using Makefile
### Set all the config parameters in Makefile

```bash
cd deploy

make all
```

### Verify Pods are running
```bash
kubectl get pods
```
Expected Output
```
NAME                                 READY   STATUS    RESTARTS      AGE
app-b768844d9-8sqt5                  1/1     Running   0             111s
cardano-db-sync-6dd8d95c65-fcqkz     1/1     Running   3 (79s ago)   111s
cardano-node-6679449bfc-vzpjr        1/1     Running   0             112s
cardano-submit-api-89cd88b58-qv5f8   1/1     Running   0             112s
cardano-wallet-7584b5c69c-525sz      1/1     Running   0             112s
pgbouncer-7dcf8687fc-z9mwb           1/1     Running   0             112s
postgresql-566586586f-nx5kp          1/1     Running   0             112s
postgrest-85b9d87cb9-dtb9b           1/1     Running   0             111s
swagger-5c464fddc6-shqt7             1/1     Running   0             111s
```

### Verify Services are running
```bash
kubectl get svc
```
Expected Output
```
app                  ClusterIP   10.104.28.13     <none>        8081/TCP   104s
cardano-submit-api   ClusterIP   10.104.142.240   <none>        8095/TCP   105s
cardano-wallet       ClusterIP   10.107.50.30     <none>        8090/TCP   105s
kubernetes           ClusterIP   10.96.0.1        <none>        443/TCP    22h
pgbouncer            ClusterIP   10.110.128.229   <none>        5432/TCP   105s
postgresql           ClusterIP   10.110.164.16    <none>        5432/TCP   105s
postgrest            ClusterIP   10.105.164.36    <none>        3000/TCP   104s
swagger              ClusterIP   10.108.137.149   <none>        8080/TCP   104s
```

## Expose service using nginx-ingress

### Install nginx-ingress-controller and verify
```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.2/deploy/static/provider/cloud/deploy.yaml

kubectl get pods --namespace=ingress-nginx
```
Note: For cloud provider like aws, ingress-controller will be installed accordingly, come with their loadbalancer (like nlb/elb in aws) 

### Apply ingress rules which redirect our traffic to app and postgrest depending on provided  URL
```bash
kubectl apply -f ingress/ingress-app.yaml

kubectl apply -f ingress/ingress-postgrest.yaml

# Check ingress
kubectl get ingress
```
Expected Output

```
NAME                       CLASS   HOSTS              ADDRESS   PORTS   AGE
demo-localhost-app         nginx   demo.localdev.me             80      47s
demo-localhost-postgress   nginx   demo.localdev.me             80      69s

```


## Testing Services
For cloud provide request will be made to loadbalancer associated with nginx-ingress-controller. For our localhost testing we request can be made on host created with ingress

### Forward a localport to ingress controller
```bash
kubectl port-forward --namespace=ingress-nginx service/ingress-nginx-controller 80:80
```

### Issue request to app service
```bash
curl demo.localdev.me/api/v1/last/tx
```
Expected Output
```
"cc416e521727a497c1a8ed0a1b363f3da6de1411daf9e2f7615f7d25657a51a0"
```

### Issue request to postgrest service
```bash
curl demo.localdev.me/api/postgrest/block?limit=2
```
Expected Output
```
[{"id":1,"hash":"\\x96fceff972c2c06bd3bb5243c39215333be6d56aaf4823073dca31afe5038471","epoch_no":null,"slot_no":null,"epoch_slot_no":null,"block_no":null,"previous_id":null,"slot_leader_id":1,"size":0,"time":"2019-07-24T20:20:16","tx_count":207,"proto_major":0,"proto_minor":0,"vrf_key":null,"op_cert":null,"op_cert_counter":null}, 
 {"id":2,"hash":"\\x8f8602837f7c6f8b8867dd1cbc1842cf51a27eaed2c70ef48325d00f8efb320f","epoch_no":0,"slot_no":null,"epoch_slot_no":null,"block_no":null,"previous_id":1,"slot_leader_id":2,"size":648085,"time":"2019-07-24T20:20:16","tx_count":0,"proto_major":0,"proto_minor":0,"vrf_key":null,"op_cert":null,"op_cert_counter":null}]
```


## Remove cluster resource
Use Makefile to remove all deployments, services, ingress and secrets etc
```bash
make remove
```

### Notes:
Currently we are using 1 pod for every container (default replicas=1), In production we will use replica according to load on any given service.

Currently we are using local docker images build on same host using ```docker build```, so we have set ```imagePullPolicy: Never``` , However in production we will use docker registry and set registry credential (using cluster management), to pull image from registry every time pipeline trigger based on changes in code.

### Architecture Diagram

Current cluster architecture diagram: 

![alt text](https://gitlab.com/migamake/cardano/streamchainquery/-/jobs/artifacts/main/raw/k8viz/bin/default.png?job=generate_k8_diagram) 
